FROM apache/airflow:2.7.2

WORKDIR /opt/airflow

USER root

RUN sudo apt-get update && sudo apt-get -y install unzip procps default-jre

USER airflow

COPY requirements.txt /
RUN pip install --upgrade --no-cache-dir pip && pip install --no-cache-dir "apache-airflow==${AIRFLOW_VERSION}" -r /requirements.txt
ENV PATH="$PATH:~/.local/bin"

# COPY --chown=airflow:root ./data_processing_dag ./dags/data_processing_dag