import numpy as np
import pandas as pd
from airflow.decorators import task

@task(task_id="process_data")
def processing(data_path, result_path):
    data = pd.read_csv(data_path)
    print(f'Successfully read dataset with len {len(data)}')
    
    total_amount = data['TransactionAmt'].sum()
    print(f'Total amount = {total_amount}')

    distinct_domains = data.drop_duplicates(['P_emaildomain'])['P_emaildomain']
    print(f'Distinct domains cnt = {distinct_domains.size}')

    important_data = data[['P_emaildomain', 'TransactionAmt']]
    important_data.to_csv(result_path)

    data = pd.read_csv(result_path)
    print(f'Successfully write dataset with len {len(data)} and {len(data.columns)} columns')

    print('Data processing finished succefully')
