from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.utils.dates import days_ago
from airflow.models.param import Param

from data_processing_dag.data_process import processing

default_args = {
    'owner': 'dangl',
    'start_date': days_ago(0),
    'depends_on_past': False,
}

with DAG(
    dag_id='data_processing_dag_1',
    default_args=default_args,
    catchup=False,
    schedule='@daily',
    tags=['process'],
    params={
        "kaggle_username": Param("", type="string"),
        "kaggle_key": Param("", type="string"),
    },
) as dag1:
    DATA_PATH = '/tmp/data_processing_dag'

    download_task = BashOperator(
        task_id="download",
        bash_command=f'rm -rf {DATA_PATH} && mkdir {DATA_PATH} && cd {DATA_PATH} && ~/.local/bin/kaggle competitions download -c hse-2021-fraud-detection',
        env={
            'KAGGLE_USERNAME': '{{ params.kaggle_username }}',
            'KAGGLE_KEY': '{{ params.kaggle_key }}'
        },
    )

    unzip_task = BashOperator(
        task_id="unzip",
        bash_command=f'cd {DATA_PATH} && unzip hse-2021-fraud-detection.zip',
    )

    process_task = processing(data_path=f'{DATA_PATH}/test_transaction.csv', result_path=f'{DATA_PATH}/important_data.csv')

    spark_job = SparkSubmitOperator(
        task_id='spark_job',
        application=f'/opt/airflow/spark/processing.py',
        name='spark_job',
        conn_id='spark',
        files=f'{DATA_PATH}/important_data.csv',
        application_args=['important_data.csv'],
    )

    download_task >> unzip_task >> process_task >> spark_job
