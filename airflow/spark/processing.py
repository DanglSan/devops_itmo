import sys
import random
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession

filename = sys.argv[1]
print(f'Spark reads data from {filename}')

conf = SparkConf().setAppName('Data processing spark app').setMaster('spark://spark-master:7077')
sc = SparkContext(conf=conf)

spark = SparkSession.builder.config(conf=conf).getOrCreate()

rdd = sc.parallelize([random.random() for _ in range(1000)])

print(f'Mean value of generated dataset: {rdd.mean()}')
print(f'Sum of generated dataset: {rdd.sum()}')
print(f'Some samples dataset: {rdd.sample(withReplacement=False, fraction=0.01).collect()}')

spark.stop()
