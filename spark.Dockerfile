FROM apache/spark:latest

USER root

RUN mkdir -p /opt/spark/logs && chmod -R 666 /opt/spark/logs

COPY ./spark-config/ $SPARK_HOME/conf/
